﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Paddle paddle1;
    [SerializeField] private Vector2 startVelocity = new Vector2(2f, 15f);
    [SerializeField] private AudioClip[] ballSounds;

    [SerializeField] private float randomFactor = 0.2f;
    
    [Header("Debug")]
    [SerializeField] private bool hasStarted;

    private Vector2 paddleToBallOffset;

    private Rigidbody2D _rigidbody2D;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _audioSource = GetComponent<AudioSource>();
        paddleToBallOffset = transform.position - paddle1.transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        if (hasStarted) return;
        LockBallToPaddle();
        LaunchBall();
    }

    private void LaunchBall()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _rigidbody2D.velocity = startVelocity;
            hasStarted = true;
        }
    }

    private void LockBallToPaddle()
    {
        var paddlePos = new Vector2(paddle1.transform.position.x, paddle1.transform.position.y);
        transform.position = paddlePos + paddleToBallOffset;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!hasStarted) return;
        var clip = ballSounds[Random.Range(0, ballSounds.Length)];
        _audioSource.PlayOneShot(clip);
        var velocityTweak = new Vector2(Random.Range(0f, randomFactor), Random.Range(0f, randomFactor));
        var currentVelocity = _rigidbody2D.velocity;
        var desiredDirection = currentVelocity + velocityTweak.normalized;
        _rigidbody2D.velocity = desiredDirection.normalized * currentVelocity.magnitude;
    }
}