﻿using UnityEngine;

public class Level : MonoBehaviour
{

    [SerializeField] private int breakableBlocks;
    private SceneLoader _sceneLoader;

    public void CountBreakableBlocks()
    {
        breakableBlocks++;
        _sceneLoader = FindObjectOfType<SceneLoader>();
    }

    public void BlockDestroyed()
    {
        breakableBlocks--;
        if (breakableBlocks <= 0)
        {
            _sceneLoader.LoadNextScene();
        }
    }
}
