﻿using UnityEngine;

public class Block : MonoBehaviour
{
    //Configuration params
    [Header("Configuration")]
    [SerializeField] private AudioClip breakSound;
    [SerializeField] private GameObject blockSparklesVFX;
    [SerializeField] private Sprite[] hitSprites;

    //State params
    [Header("Debug")]
    [SerializeField] private int currentHits;
    
    //Cached components
    private GameState _gameState;
    private Level _level;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _gameState = FindObjectOfType<GameState>();
        _level = FindObjectOfType<Level>();
        if (CompareTag("Breakable"))
        {
            _level.CountBreakableBlocks();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (CompareTag("Breakable"))
        {
            BlockHit();
        }
    }

    private void BlockHit()
    {
        currentHits++;
        var maxHits = hitSprites.Length + 1;
        if (currentHits >= maxHits)
        {
            DestroyBlock();
        }
        else
        {
            ShowNextHitSprite();
        }
    }

    private void ShowNextHitSprite()
    {
        var index = currentHits - 1;
        _spriteRenderer.sprite = hitSprites[index];
    }

    private void DestroyBlock()
    {
        AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position);
        _level.BlockDestroyed();
        _gameState.AddToScore();
        TriggerParticlesVFX();
        
        Destroy(gameObject);
    }

    private void TriggerParticlesVFX()
    {
        var sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
        Destroy(sparkles, 2f);
    }
}