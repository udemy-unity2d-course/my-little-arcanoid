﻿using UnityEngine;

public class Paddle : MonoBehaviour
{

    [SerializeField] private float screenWidthInUnits = 16f;
    [SerializeField] private float leftLimitInUnits = 1f;
    [SerializeField] private float rightLimitInUnits = 15f;

    private GameState _gameState;
    private Ball _ball;

    private void Start()
    {
        _gameState = FindObjectOfType<GameState>();
        _ball = FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    private void Update()
    {
        var mouseXPos = Mathf.Clamp(getXPos(), leftLimitInUnits, rightLimitInUnits);
        transform.position = new Vector2(mouseXPos, transform.position.y);
    }

    private float getXPos()
    {
        if (_gameState.IsAutoPlayEnabled)
        {
            return _ball.transform.position.x;
        }
        else
        {
            return Input.mousePosition.x / Screen.width * screenWidthInUnits;
        }
    }
}
